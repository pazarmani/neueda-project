# neueda-project

Pasos para la configuración e instalación:

- BASE DE DATOS
- REPOSITORIO
- BACK
- DOCKER


******************************************************************
	BASE DE DATOS
******************************************************************

MySql version 5.6

1) Conectarse a mysql (con usuario root)
/usr/local/mysql/bin/mysql -h localhost -u root -p

2) Crear schema dev_neueda
CREATE DATABASE dev_neueda character set utf8;

2-a) Generación de clave admin encriptada 
SELECT @password:=PASSWORD('xxxxx');

> 
| @password:=PASSWORD('xxxxx')              |
| *xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx|
> 
2-b) Crear user para dev_neueda con la clave encriptada 
CREATE USER 'admin_xxx'@'localhost' IDENTIFIED BY PASSWORD '*xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';

2-c) Asignar privilegios al user 'admin_xxx'
GRANT ALL ON dev_neueda.* TO 'admin_xxx'@'localhost';

2-d) Confirmar todos los privilegios a ese user
FLUSH PRIVILEGES;

3) Conectarse con usuario:'admin_xxx' y pass: xxxxx
/usr/local/mysql/bin/mysql -u admin_xxx -p dev_neueda
Password: xxxxx


******************************************************************
	REPOSITORIO - GitLab
******************************************************************

1) Crear user en GitLab en caso que no tengas.

2) Generar la key y configurarla.

4) Clonar el repositorio:
	git clone git@gitlab.com:pazarmani/neueda-project.git


******************************************************************
	BACK
******************************************************************

1) JDK 8

2) maven 3.5.0 

3) eclipse, versión sugerida oxygen. “Import maven project” al proyecto que fué generado desde spring boot (Spring HATEOAS, Spring Data JPA, MySql Driver, Spring Configuration Processor).


******************************************************************
	DOCKER
******************************************************************

1) Instalar Docker. Entrar a https://hub.docker.com/:

	docker pull pacita/neueda-back


	docker pull pacita/neueda-mysql



2) Generar el container desde la imagen descargada:

	docker run --name neueda-mysql -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=dev_neueda -e MYSQL_USER=admin_xxx -e MYSQL_PASSWORD=xxxxx -d pacita/neueda-mysql

	docker run -p 8086:8086 --name neueda-back --link neueda-mysql:mysql -d pacita/neueda-back
