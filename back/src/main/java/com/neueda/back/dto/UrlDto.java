package com.neueda.back.dto;

import java.util.Date;

/**
 * 
 * @author pacita
 *
 */
public class UrlDto {

	private String id;

	private String longUrl;

	private Date created;

	private Date updated;

	public UrlDto() {
	}

	public UrlDto(String id, String longUrl) {
		this.id = id;
		this.longUrl = longUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLongUrl() {
		return longUrl;
	}

	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}