package com.neueda.back.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.FieldError;

/**
 * 
 * @author pacita
 *
 */
public class ResponseMessageDto {

	private String message;
	private String code;
	private List<String> detail = new ArrayList<String>();
	private FieldError[] fieldErrors;

	/**
	 * 
	 * @param message
	 */
	public ResponseMessageDto(String message) {
		this.message = message;
	}

	/**
	 * 
	 * @param code
	 * @param message
	 */
	public ResponseMessageDto(String code, String message) {
		this.code = code;
		this.message = message;
	}

	/**
	 * 
	 * @param fieldErrors
	 */
	public ResponseMessageDto(FieldError[] fieldErrors) {
		this.message = "Request not valid";
		if (fieldErrors != null) {
			for (FieldError error : fieldErrors) {
				this.detail.add(error.getField() + ": " + error.getDefaultMessage());
			}
		}
		this.fieldErrors = fieldErrors;
	}

	public String getMessage() {
		return message;
	}

	public String getCode() {
		return code;
	}

	public List<String> getDetail() {
		return detail;
	}

	public void setDetail(List<String> detail) {
		this.detail = detail;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public FieldError[] getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(FieldError[] fieldErrors) {
		this.fieldErrors = fieldErrors;
	}

}