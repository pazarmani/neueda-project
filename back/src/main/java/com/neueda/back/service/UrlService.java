package com.neueda.back.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.neueda.back.entity.Url;
import com.neueda.back.exception.ServiceException;
import com.neueda.back.repository.UrlRepository;
import com.neueda.back.util.ConstantsUtils;
import com.neueda.back.util.Md5;

/**
 * 
 * @author pacita
 *
 */
@Service
@Transactional
public class UrlService {

	@Autowired
	UrlRepository urlRepository;

	/**
	 * Metodo encargado de recuperar el objeto url por su Id
	 * 
	 * @param id
	 * @return
	 * @throws ServiceException
	 */
	public Url getById(String id) throws ServiceException {
		Optional<Url> optional = urlRepository.findById(id);
		return optional.orElse(null);
	}

	/**
	 * Metodo encargado de crear un objeto url.
	 * 
	 * @param url
	 * @return
	 * @throws ServiceException
	 */
	public Url create(Url url) throws ServiceException {
		if (url == null) {
			throw new ServiceException("Error creating the Url");
		}

		String longUrl = url.getLongUrl();

		int startIndex = 0;
		int endIndex = startIndex + ConstantsUtils.URL_ID_SIZE - 1;

		return recursiveCreate(longUrl, startIndex, endIndex);
	}

	/**
	 * Metodo encargado de crear url con id único recursivamente. Si no existe el
	 * Id, lo crea. Y sino compara el longUrl y si coincide, devuelve la url con id
	 * ya creado anteriormente.
	 * 
	 * @param longUrl
	 * @param startIndex
	 * @param endIndex
	 * @return
	 * @throws ServiceException
	 */
	private Url recursiveCreate(String longUrl, int startIndex, int endIndex) throws ServiceException {

		longUrl = longUrl.replaceAll(ConstantsUtils.BREAKING_CHARACTERS, "_");

		String id = Md5.generate(longUrl, startIndex, endIndex);

		Url url = getById(id);
		if (null == url) {
			url = urlRepository.save(new Url(id, longUrl));
		} else if (!url.getLongUrl().equals(longUrl)) {
			url = recursiveCreate(longUrl, startIndex + 1, endIndex + 1);
		}

		return url;
	}

	/**
	 * Metodo encargado de eliminar el objeto url
	 * 
	 * @param id
	 * @throws ServiceException
	 */
	public void delete(String id) throws ServiceException {
		getById(id);
		urlRepository.deleteById(id);
	}

	/**
	 * Metodo encargado de recuperar todos los objetos url
	 * 
	 * @return
	 */
	public List<Url> findAll() {
		List<Url> result = urlRepository.findAll();
		return result;
	}
}