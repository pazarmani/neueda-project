package com.neueda.back.util;

/**
 * 
 * @author pacita
 *
 */
public final class ConstantsUtils {

	public final static int URL_ID_SIZE = 6;
	public final static String MD5_ALGORITHM_EXCEPTION = "MD5 is not available";
	public static final String BREAKING_CHARACTERS = "[\n|\r|\t]";

	public final static class CODE {
		public final static String SUCCESS = "SUCCESS";
		public final static String WARNING = "WARNING";
		public final static String ERRORS = "ERRORS";
	}

	public final static class MESSAGES {

		public final static class SUCCESS {
			// URL
			public final static String URL_001 = "MESSAGES.SUCCESS.URL_001";
		}

		public final static class WARNING {
			// URL
			public final static String URL_001 = "MESSAGES.WARNING.URL_001";
		}

		public final static class ERRORS {
			// URL
			public final static String URL_001 = "MESSAGES.ERRORS.URL_001";

		}

	}

}