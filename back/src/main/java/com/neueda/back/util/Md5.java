package com.neueda.back.util;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import com.neueda.back.exception.UrlMd5Exception;

/**
 * 
 * @author pacita
 *
 */
public class Md5 {

	private static final int RADIX = 16;
	private static final int HASH_LENGTH = 32;
	private static final String MD5_ALGORITHM = "MD5";

	/**
	 * Codificación base64 en el hash para generar la cadena y tomar los primeros 6
	 * caracteres. Se reemplazan los caracteres / y + de la codificación Base64.
	 * 
	 * @param longUrl
	 * @param startIndex
	 * @param endIndex
	 * @return
	 */
	public static String generate(String longUrl, int startIndex, int endIndex) {
		String md5 = getMd5(longUrl);
		String base64 = getBase64(md5);
	//Base64 no es seguro para URL ya que contiene / y +. 
	//Así que reemplazaremos estos caracteres de la codificación Base64.
		String hash = base64.replace('/', '_').replace('+', '-');
//substring extrae caracteres desde startIndex hasta endIndex sin incluirlo.
		return hash.substring(startIndex, endIndex + 1);
	}

	
	/**
	 * para generar la cadena y tomar los primeros 6 caracteres
	 * @param md5
	 * @return
	 */
	private static String getBase64(String md5) {
		return Base64.getEncoder().encodeToString(md5.getBytes());
	}

	/**
	 * 
	 * @param longUrl
	 * @return
	 */
	private static String getMd5(String longUrl) {
		try {
			byte[] message = MessageDigest.getInstance(MD5_ALGORITHM).digest(longUrl.getBytes());

			String hash = new BigInteger(1, message).toString(RADIX);
			hash = hash.length() < HASH_LENGTH ? "0".concat(hash) : hash;

			return hash;
		} catch (NoSuchAlgorithmException e) {
			throw new UrlMd5Exception(ConstantsUtils.MD5_ALGORITHM_EXCEPTION, e);
		}
	}

}
