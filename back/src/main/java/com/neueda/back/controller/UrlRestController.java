package com.neueda.back.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.neueda.back.dto.ResponseMessageDto;
import com.neueda.back.dto.UrlDto;
import com.neueda.back.entity.Url;
import com.neueda.back.exception.APIException;
import com.neueda.back.exception.ServiceException;
import com.neueda.back.service.UrlService;

/**
 * 
 * @author pacita
 *
 */
@RestController
@RequestMapping("/url")
public class UrlRestController {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	UrlService urlService;

	/**
	 * Método encargado de redireccionar a la URL larga.
	 * 
	 * @param id (url corta)
	 * @return
	 * @throws APIException
	 */
	@GetMapping("/{id}")
	public ResponseEntity<Url> redirect(@PathVariable String id) throws APIException {

		Url url;
		try {
			url = urlService.getById(id);
		} catch (ServiceException e) {
			throw new APIException(e);
		}

		HttpHeaders headers = new HttpHeaders();
		//nueva ubicacion
		headers.setLocation(URI.create(url.getLongUrl()));

		return new ResponseEntity<>(headers, HttpStatus.MOVED_PERMANENTLY);
	}


	
	/**
	 * Método encargado de crear un objeto url.
	 * 
	 * @param urlDto
	 * @return
	 * @throws APIException
	 */
	@PostMapping
	public ResponseMessageDto createUrl(@Valid @RequestBody UrlDto urlDto) throws APIException {

		try {
			urlService.create(toEntity(urlDto));
		} catch (ServiceException e) {
			throw new APIException(e);
		}
		return new ResponseMessageDto("Url created");
	}

	/**
	 * Método encargado de borrar el objeto url.
	 * 
	 * @param id
	 * @return
	 * @throws APIException
	 */
	@DeleteMapping("/{id}")
	public ResponseMessageDto deleteUrl(@PathVariable String id) throws APIException {
		try {
			urlService.delete(id);
		} catch (ServiceException e) {
			throw new APIException(e);
		} catch (Exception e) {
			throw new APIException(e);
		}
		return new ResponseMessageDto("Url deleted");
	}

	/**
	 * Método encargado de recuperar todos los obetos url.
	 * 
	 * @return
	 */
	@GetMapping("/all")
	public List<UrlDto> loadAll() {
		return urlService.findAll().stream().map(url -> toDto(url)).collect(Collectors.toList());
	}

	/**
	 * Mapea un UrlDto entity a Url entity
	 * @param urlDto
	 * @return
	 * @throws APIException
	 */
	private Url toEntity(UrlDto urlDto) throws APIException {
		Url url = new Url();
		if (urlDto.getId() != null) {
			try {
				url = urlService.getById(urlDto.getId());
			} catch (ServiceException e) {
				throw new APIException(e);
			}
		}
		modelMapper.map(urlDto, url);
		return url;
	}

	/**
	 * Mapea un Url entity a UrlDto
	 * @param url
	 * @return urlDto
	 */
	private UrlDto toDto(Url url) {
		return modelMapper.map(url, UrlDto.class);
	}
}