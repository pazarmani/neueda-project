package com.neueda.back.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import com.neueda.back.util.ConstantsUtils;

/**
 * 
 * @author pacita
 *
 */
@Entity
@Table(name = "URL")
public class Url implements Serializable {

	private static final long serialVersionUID = -5214729908914776736L;

	@Id
	@Column(length = ConstantsUtils.URL_ID_SIZE)
	private String id;

	@Column
	private String longUrl;

	@Column
	@CreationTimestamp
	private Date created;

	@UpdateTimestamp
	private Date updated;

	public Url() {
		super();
	}

	public Url(String id, String longUrl) {
		super();
		this.id = id;
		this.longUrl = longUrl;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLongUrl() {
		return longUrl;
	}

	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

}
