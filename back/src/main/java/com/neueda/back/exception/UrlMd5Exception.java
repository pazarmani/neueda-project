package com.neueda.back.exception;

/**
 * 
 * @author pacita
 *
 */
public class UrlMd5Exception extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9173197634848438913L;

	public UrlMd5Exception(String message) {
		super(message);
	}

	public UrlMd5Exception(String message, Throwable cause) {
		super(message, cause);
	}
}
