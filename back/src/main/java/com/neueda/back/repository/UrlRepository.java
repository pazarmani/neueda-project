package com.neueda.back.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.neueda.back.entity.Url;

/**
 * 
 * @author pacita
 *
 */
@Repository
@Transactional
public interface UrlRepository extends JpaRepository<Url, String> {

}